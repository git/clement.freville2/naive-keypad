# naive-keypad

An interactive demonstration of a keypad with a quickly discoverable code.

## Deploying

To deploy it on your web server, you need to copy those files/directories:

- `index.html`
- `src/`

## Developing

You may optionally use a build step to minify the code and have a development server.

Ensure you have [Node.js](https://nodejs.org) installed. Then run your favorite package manager (npm, yarn, pnpm, bun, etc.) to install the dependencies and build the application:

```sh
npm install
npm run dev # Run the Vite development server
npm run build # Build the application in the dist/ directory
```
